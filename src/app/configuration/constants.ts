export class Constants {

    /**
     * Valeurs par défaut pour les besoins d'authentifications
     */
    public static REDIRECT_URI_DEFAULT         : string = 'http://localhost:4200/authentification';
    public static RESPONSE_TYPE_DEFAULT        : string = 'code';
    public static GRANT_TYPE_FIRST_CODE_DEFAULT: string = 'authorization_code';
    public static APPROVAL_PROMPT_DEFAULT      : string = 'force';
    public static SCOPE_DEFAULT                : string = 'profile:read_all,profile:write,activity:read_all,activity:write'; 

    /**
     * noms des params dans les requètes URL 
     */
    public static CLIENT_ID_PARAM       : string = 'client_id';
    public static CLIENT_SECRET_PARAM   : string = 'client_secret';
    public static CLIENT_CODE_PARAM     : string = 'code';
    public static REDIRECT_URI_PARAM    : string = 'redirect_uri';
    public static RESPONSE_TYPE_PARAM   : string = 'response_type';
    public static APPROVAL_PROMPT_PARAM : string = 'approval_prompt';
    public static SCOPE_PARAM           : string = 'scope';
    public static GRANT_TYPE_PARAM      : string = 'grant_type';

    /**
     * Cookies
     */
    // Client ID
    public static CLIENT_ID_COOKIE    : string = "client_id_strava"
    // client secret
    public static CLIENT_SECRET_COOKIE: string = "client_secret_strava"
    // Nom du access token
    public static ACCESS_TOKEN_COOKIE : string = "access_token_strava";
    // Nom du refresh Token
    public static REFRESH_TOKEN_COOKIE: string = "refresh_token_strava";
    // Nom du refresh Token
    public static CODE_COOKIE         : string = "code_strava";
    // Nom du refresh Token
    public static EXPIRES_AT_COOKIE   : string = "expires_at_strava";
    
    /**
     * URLs Strava
     */
    // URL de connexion strava pour obtenir les droits d'accès à l'application
    public static URL_AUTHORIZE    : string = 'https://www.strava.com/oauth/authorize';
    // URL d'obtention de token
    public static URL_TOKEN: string = 'https://www.strava.com/oauth/token'; 
}