import { Utils } from './utils'

describe('getTokenExpirationTime', ()=>{
    it('devrait retourner null', ()=>{
        let value = Utils.getTokenExpirationTime(-1);
        expect(value).toBeNull();
    })

    it('devrait retourner une date', ()=>{
        let value = Utils.getTokenExpirationTime(123456789);
        expect(value).toBeGreaterThan(0);
    })
})

describe('isAccessTokenExpired', ()=>{
    it('devrait renvoyer null', ()=>{
        let value = Utils.isAccessTokenExpired(-1);
        expect(value).toBeNull(); 
    })

    it('devrait renvoyer true', ()=>{
        let value = Utils.isAccessTokenExpired(0);
        expect(value).toBeTruthy();
    })
})