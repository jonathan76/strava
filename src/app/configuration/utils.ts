export class Utils{

  // Détermine si le token est expiré ou pas
  static isAccessTokenExpired(p_expiration: number):boolean{
    if (p_expiration <0) return null;
    if (p_expiration){
      return (Date.now()>p_expiration)
    }else{
      return true;
    }
  }

  // Retourne la date et heure d'expiration du token sous forme de date
  static getTokenExpirationTime(p_expiration: number):Date{
    if (p_expiration <0) return null;
    // Définit la date au 1er janvier 1970
    let dateTemp = new Date(0);
    dateTemp.setUTCSeconds(p_expiration);
    return dateTemp;
  }
}