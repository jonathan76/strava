import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Configuration } from '../../stravaTSAngular';
import { Oauth2Service } from '../../services/oauth2.service';
import { Constants } from '../../configuration/constants'
import { Router, ActivatedRoute, ParamMap } from '@angular/router';


@Component({
  selector: 'app-oauth',
  templateUrl: './oauth.component.html',
  styleUrls: ['./oauth.component.css']
})
export class OauthComponent implements OnInit {

  formAuth: FormGroup;
  config  : Configuration = new Configuration();
  data;

  constructor(
    private formBuilder: FormBuilder,
    private authService: Oauth2Service,
    private route      : ActivatedRoute
  ) { }

  ngOnInit() {
    // Récupération du code si l'on vient de s'authentifier
    this.route.queryParamMap.subscribe(paramMap =>{
      if (paramMap.get('code')){
        this.config.code = paramMap.get('code')
        this.config.saveCodeToLocalStorage(this.config.code);        
      }
    })
    
    // Valeurs par défaut dans le formulaire
    this.formAuth = this.formBuilder.group({
      client_id      : [this.config.client_id, Validators.required],
      redirect_uri   : [Constants.REDIRECT_URI_DEFAULT, Validators.required],
      response_type  : [Constants.RESPONSE_TYPE_DEFAULT, Validators.required],
      approval_prompt: [Constants.APPROVAL_PROMPT_DEFAULT],
      scope          : [Constants.SCOPE_DEFAULT],
      client_secret  : [this.config.client_secret, Validators.required],
      refresh_token  : [this.config.refresh_token], 
    })
  }

  // Validation du formulaire
  validForm(){
    let form = this.formAuth.value;
    // Récupération des valeurs saisies
    this.config.client_id       = form['client_id']; 
    this.config.redirect_uri    = form['redirect_uri'];
    this.config.response_type   = form['response_type'];
    this.config.approval_prompt = form['approval_prompt'];
    this.config.scope           = form['scope'];  
    this.config.client_secret   = form['client_secret'];
    this.config.refresh_token   = form['refresh_token'];
    this.config.saveConfigToLocalStorage();
    this.authService.authenticate(this.config)
  }

}
