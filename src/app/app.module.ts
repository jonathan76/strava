import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms' 
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { HttpClientModule ,HttpClient } from '@angular/common/http';
import { AthletesService } from './stravaTSAngular/api/athletes.service';
import { OauthComponent } from './components/oauth/oauth.component';
import { MatFormFieldModule, MatAutocompleteModule, MatBadgeModule, MatBottomSheetModule, MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatChipsModule,
  MatDatepickerModule, MatDialogModule, MatDividerModule, MatExpansionModule, MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatNativeDateModule,
  MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatRippleModule, MatSelectModule, MatSidenavModule, MatSliderModule, MatSlideToggleModule,
  MatSnackBarModule, MatSortModule, MatStepperModule, MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule, MatTreeModule,
 } from '@angular/material';
import { Oauth2Service } from './services/oauth2.service';
import { ActivitiesService } from './stravaTSAngular';

const routes = [
  { path: 'authentification', component : OauthComponent }, 
  { path: '', redirectTo : '/', pathMatch : 'full' },
  { path : '**', redirectTo : '/' }
]

@NgModule({
  declarations: [
    AppComponent,
    OauthComponent,
     
  ],
  imports: [
    BrowserModule, MatNativeDateModule, HttpClientModule, ReactiveFormsModule, BrowserAnimationsModule,
    MatInputModule, MatFormFieldModule, MatButtonModule, MatSnackBarModule, MatIconModule, MatAutocompleteModule, MatBadgeModule, MatBottomSheetModule, MatButtonToggleModule, MatCardModule, 
    MatCheckboxModule, MatChipsModule, MatDialogModule, MatTableModule, MatDatepickerModule, MatDividerModule, MatExpansionModule, MatGridListModule, MatListModule, MatMenuModule, MatPaginatorModule,
    MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatRippleModule, MatSelectModule, MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSortModule, MatStepperModule,
    MatTabsModule, MatToolbarModule, MatTooltipModule, MatTreeModule,
    RouterModule.forRoot(routes),
  ],
  providers: [
    HttpClient,
    Oauth2Service,
    AthletesService,
    ActivitiesService
  ],
  bootstrap: [AppComponent] 
})
export class AppModule { }
