import { Component, OnInit } from '@angular/core';
// Interfaces Strava
import { DetailedAthlete } from './stravaTSAngular/model/detailedAthlete'
import { ActivitiesService, PolylineMap } from './stravaTSAngular';
import { AthletesService } from './stravaTSAngular/api/athletes.service'
import * as L from 'leaflet'
import * as Polyline from 'polyline'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title  = 'Strava';
  athlete: DetailedAthlete
  map : PolylineMap

  constructor(
    private service : AthletesService,
    private activityService : ActivitiesService
  ){}

  ngOnInit(){
  }
 
  test(){
    this.service.getLoggedInAthlete().subscribe(data=>{
      this.athlete = data;
      this.activityService.getLoggedInAthleteActivities().subscribe(activities=>{
        this.map = activities[0]['map'];
        let map = L.map('map').setView(
          [49.61, 0.84], 14
        )
        L.tileLayer(
          'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
              maxZoom: 25,
          }).addTo(map);
        let coords = Polyline.decode(this.map.summary_polyline)
        // console.log(coords) 
        L.polyline(coords, { color: 'violet', lineJoin: 'round' }).addTo(map);
      })
      // this.service.getStats(this.athlete.id).subscribe(data=>{
      //   console.log(data)
      // }) 
    })
  }
}
