import { Injectable } from '@angular/core';
import { Configuration } from '../stravaTSAngular';
import { HttpClient } from '@angular/common/http'
import { Constants } from '../configuration/constants'
import { Utils } from '../configuration/utils';

@Injectable({
  providedIn: 'root'
})
export class Oauth2Service {

  config: Configuration = new Configuration();

  constructor(
    private http : HttpClient
  ) {
    this.obtainAccessToken();
   }

  // lance la procédure d'authentification
  authenticate(p_config?: Configuration){
    
    // Si une config est en paramètre, on la place dans le service
    if (p_config) this.config = p_config;
    // Ouvre une nouvelle fenêtre pour obtenir les autorisations
    window.open(`${Constants.URL_AUTHORIZE}?client_id=${this.config.client_id}&redirect_uri=${this.config.redirect_uri}&approval_prompt=${this.config.approval_prompt}&response_type=${this.config.response_type}&scope=${this.config.scope}`)
    
  }

  // Récupère un token une fois authentifié
  obtainAccessToken(p_config?: Configuration){
    if (p_config) this.config = p_config;

    // Obtention du token
    this.http.post(Constants.URL_TOKEN, this.config).subscribe(data=>{
      this.config.accessToken=data['access_token'];
      this.config.refresh_token=data['refresh_token'];
      this.config.expires_at=data['expires_at'];
      this.config.saveConfigToLocalStorage();
    }, error=>{
      console.log('une erreur est survenue :')
      console.log(error)
    })
  }

  // Gère les tokens
  manageTokens(){
    // Si on a un access_token
    if (this.config.accessToken){
        // On verifie s'il est toujours valide
        if (Utils.isAccessTokenExpired(this.config.expires_at)){
            // Si on a un refresh_token
            if (this.config.refresh_token){
              // On utilise le refresh_token pour obtenir un nouveau access_token
              this.obtainAccessToken();
            }
        }
    }else{
        // Si on a un refresh_token
        if (this.config.refresh_token){
          // On utilise le refresh_token pour obtenir un nouveau access_token
          this.obtainAccessToken();
        }else{
            // Si on a un code
            this.obtainAccessToken();
        }
    }
  }
}
